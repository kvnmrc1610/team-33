"use strict";
bucketingTime()
let content = document.getElementById("content");
let myHTML = "";

function sorting(sorted,unsorted){
    let index = [];
    for(let i =0;i<unsorted.length;i++){
    index.push(unsorted.indexOf(sorted[i]))
    }
    return index;
    
}
for(var i in bucket)
{   
    let occupancy=[];
    let dummyOccupancy = [];
    let occupancySorted = [];
    let occupancyUnsorted = [];
    let newIndex = [];
    
    
   myHTML += `
                    <div class="mdl-cell mdl-cell--4-col">
                        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                            <thead>
                                <tr><th class="mdl-data-table__cell--non-numeric">
                                    <h5>Worst occupancy for ${i}</h5>
                                </th></tr>
                            </thead>
                            <tbody>
`  
      
    for (let j=0;j<bucket[i]._roomList.length;j++){
        occupancy.push((bucket[i]._roomList[j]._seatsUsed/bucket[i]._roomList[j]._seatsTotal*100).toFixed(2));
       dummyOccupancy.push((bucket[i]._roomList[j]._seatsUsed/bucket[i]._roomList[j]._seatsTotal*100).toFixed(2));
      

    }
     
    occupancyUnsorted = occupancy;
    occupancySorted = dummyOccupancy.sort((a,b) => a-b)
    newIndex = sorting(occupancySorted,occupancyUnsorted)
    
    
    if(bucket[i]._roomList.length > 6){
    for (let k=0;k < 5;k++){
        let time = new Date (bucket[i]._roomList[newIndex[k]]._timeChecked)
        let date = time.getDate()
        let month = time.getMonth()
        let year = time.getFullYear()
        let hour = time.getHours()
        let minute = time.getMinutes()
        let second = time.getSeconds()
        myHTML += `   
                                <tr><td class="mdl-data-table__cell--non-numeric" >
                                    <div><b>${bucket[i]._roomList[newIndex[k]]._address.split(',')[0]}; Rm ${bucket[i]._roomList[newIndex[k]]._roomNumber}</b></div>
                                    <div>Occupancy: ${occupancySorted[k]}%</div>
                                    <div>Heating/cooling: ${convertOnOff(bucket[i]._roomList[newIndex[k]]._heatingCoolingOn)}</div>
                                    <div>Lights: ${convertOnOff(bucket[i]._roomList[newIndex[k]]._lightsOn)}</div>
                                    <div><font color="grey"><i>${date}/${monthNum[month]}/${year}, ${hour}:${minuteminuteNum[minute]}:${second}</i></font></div>
                                </td></tr>`
        
        }
    }
     else {
        for (let k=0;k < bucket[i]._roomList.length;k++){
        let time = new Date (bucket[i]._roomList[newIndex[k]]._timeChecked)
        let date = time.getDate()
        let month = time.getMonth()
        let year = time.getFullYear()
        let hour = time.getHours()
        let minute = time.getMinutes()
        let second = time.getSeconds()
        myHTML += `   
                                <tr><td class="mdl-data-table__cell--non-numeric" >
                                    <div><b>${bucket[i]._roomList[newIndex[k]]._address.split(',')[0]}; Rm ${bucket[i]._roomList[newIndex[k]]._roomNumber}</b></div>
                                    <div>Occupancy: ${occupancySorted[k]}%</div>
                                    <div>Heating/cooling: ${convertOnOff(bucket[i]._roomList[newIndex[k]]._heatingCoolingOn)}</div>
                                    <div>Lights: ${convertOnOff(bucket[i]._roomList[newIndex[k]]._lightsOn)}</div>
                                    <div><font color="grey"><i>${date}/${monthNum[month]}/${year}, ${hour}:${minuteNum[minute]}:${second}</i></font></div>
                                </td></tr>`
       }
    }
    
    myHTML += `     </tbody>
                        </table>
  </div>`
    
}

 content.innerHTML  = myHTML;
