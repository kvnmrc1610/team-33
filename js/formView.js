//Saving form into the local Storage when
function save()
{
    address.value=address.value.split(',')[0];
    if (heatingCoolingOn.checked == true)
    {
        heatingCoolingOn.value = "On"
    } 
    else
    {
        heatingCoolingOn.value = "Off"
    }
    
    if (lightsOn.checked == true)
    {
        lightsOn.value = "On"
    } 
    else
    {
        lightsOn.value = "Off"
    }
    
    if (address.value == "" || roomNumber.value == "" || seatsUsed.value == "" || seatsTotal.value == "")
    {
        displayMessage("A required field is missing.",2000)
        return false
    } 
    else if (Number(seatsUsed.value) > Number(seatsTotal.value))
    {
        displayMessage("Available seats should be greater than seats in use",2000)
        return false
    }
    else 
    {
        let myRoom = new RoomUsage(address.value,roomNumber.value,lightsOn.value,heatingCoolingOn.value,Number(seatsUsed.value),Number(seatsTotal.value));
        roomList._roomList.push(myRoom)
        localStorage.setItem("ENG1003-RoomUseList",JSON.stringify(roomList));
        displayMessage("Observation saved.",2000)
    }
    setTimeout(()=>
    {
        window.location.reload();
    },900
              );
    
};

//Clear form when press clear button
function clearForm()
{
    location.reload()
}
//Automatically determine address
function autoAddress() 
{
    if (document.getElementById('_useAddress').checked){
        displayMessage("Tracking location.",3000)
    if (navigator){
         navigator.geolocation.getCurrentPosition(position => {
            var apikey = '95bd165b3a614c41bf059ea26fef8644';
            const latitude = position.coords.latitude;
            const longitude = position.coords.longitude;
            var api_url = 'https://api.opencagedata.com/geocode/v1/json'

  var request_url = api_url
    + '?'
    + 'key=' + apikey
    + '&q=' + encodeURIComponent(latitude + ',' + longitude)
    + '&pretty=1'
    + '&no_annotations=1';

  // see full list of required and optional parameters:
  // https://opencagedata.com/api#forward

  var request = new XMLHttpRequest();
  request.open('GET', request_url, true);

  request.onload = function() {
    // see full list of possible response codes:
    // https://opencagedata.com/api#codes

    if (request.status == 200){ 
      
        // Success!
      var data = JSON.parse(request.responseText);
        if(data.results[0].confidence > 7){
        
            if(data.hasOwnProperty('footway')){

                    document.getElementById('_address').value=data.results[0].components.footway;
                    document.getElementById('_label').style.display='none'

            }else{

                document.getElementById('_address').value=data.results[0].components.road;
                document.getElementById('_label').style.display='none'
                }
        
        }
        else {
            alert('Location is Inaccurate please fill the address manually')
        }
        
    } else if (request.status <= 500) { 
      
    // We reached our target server, but it returned an error
                           
      console.log("unable to geocode! Response code: " + request.status);
      
      var data = JSON.parse(request.responseText);
      console.log(data.status.message);
    
    } else {
     
        console.log("server error");
    }
  };

  request.onerror = function() {
    
      // There was a connection error of some sort
    console.log("unable to connect to server");        
  
  };

  request.send();  // make the request
    });
        
    } 
    else {
        console.log("Geolocation is not supported by this browser.");
    }
    }
}
