"use strict";
let allStorage1 = JSON.parse(localStorage.getItem("ENG1003-RoomUseList"))
bucketingBuilding();
let data1 = document.getElementById("content1");

for(var i in bucket)
{
    let totalLightsOn = 0
    let totalHeatingCoolingOn = 0
    let numberObservation = bucket[i]._roomList.length
    let wastefulObs = 0
    let totalSeatsUsedArr=[];
    let totalSeatsUsed
    let totalSeatsTotalArr=[];
    let totalSeatsTotal

    for(let j=0;j<bucket[i]._roomList.length;j++)
    {  
        convertOnOff(bucket[i]._roomList[j]._lightsOn)
        convertOnOff(bucket[i]._roomList[j]._heatingCoolingOn)
        totalSeatsUsedArr.push(parseFloat(bucket[i]._roomList[j]._seatsUsed))
        totalSeatsUsed = totalSeatsUsedArr.reduce((a, b) => a + b, 0)
        totalSeatsTotalArr.push(parseFloat(bucket[i]._roomList[j]._seatsTotal))
        totalSeatsTotal = totalSeatsTotalArr.reduce((a, b) => a + b, 0)
        if(bucket[i]._roomList[j]._lightsOn === 'On')
        {
            totalLightsOn++
        }
        
        if(bucket[i]._roomList[j]._heatingCoolingOn === 'On')
        {
            totalHeatingCoolingOn++
        }
        
        if((bucket[i]._roomList[j]._heatingCoolingOn === "On" && bucket[i]._roomList[j]._seatsUsed == '0' ) || (bucket[i]._roomList[j]._lightsOn=== 'On' &&                    bucket[i]._roomList[j]._seatsUsed == '0') )
        {
            wastefulObs++
        }
        
          if((bucket[i]._roomList[j]._heatingCoolingOn === true && bucket[i]._roomList[j]._seatsUsed == '0' ) || (bucket[i]._roomList[j]._lightsOn=== true &&                    bucket[i]._roomList[j]._seatsUsed == '0') )
        {
            wastefulObs++
        }
        
    }
    
    if(wastefulObs > 0)
    {
          data1.innerHTML +=
    
        ` <div class="mdl-cell mdl-cell--4-col" id = "cell1${i}">
    <table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">
        <thead>
            <tr><th class="mdl-data-table__cell--non-numeric" >
                <h4 id = title${bucket[i]}>
                    ${i.split(',')[0]}
                </h4>
            </th></tr>
        </thead>
        <tbody>
            <tr><td class="mdl-data-table__cell--non-numeric" style="background-color:Yellow;">
                Observations: ${bucket[i]._roomList.length}<br />
                Wasteful observations: ${wastefulObs} <br />
                Average seat utilisation: ${totalSeatsUsed/totalSeatsTotal*100}%<br />
                Average lights utilisation: ${totalLightsOn/numberObservation*100}%<br />
                Average heating/cooling utilisation: ${totalHeatingCoolingOn/numberObservation*100}%
            </td></tr>
        </tbody>
    </table>
</div>
`
    } 
    else
    {
      data1.innerHTML +=
    
        ` <div class="mdl-cell mdl-cell--4-col" id = "cell1${i}">
    <table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">
        <thead>
            <tr><th class="mdl-data-table__cell--non-numeric">
                <h4 id = title${bucket[i]}>
                    ${i.split(',')[0]}
                </h4>
            </th></tr>
        </thead>
        <tbody>
            <tr><td class="mdl-data-table__cell--non-numeric">
                Observations: ${bucket[i]._roomList.length}<br />
                Wasteful observations: ${wastefulObs} <br />
                Average seat utilisation: ${totalSeatsUsed/totalSeatsTotal*100}%<br />
                Average lights utilisation: ${totalLightsOn/numberObservation*100}%<br />
                Average heating/cooling utilisation: ${totalHeatingCoolingOn/numberObservation*100}%
            </td></tr>
        </tbody>
    </table>
</div>
`
    }
}