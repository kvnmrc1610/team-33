"use strict";
let address = document.getElementById("_address");
let roomNumber = document.getElementById("_roomNumber");
let seatsTotal = document.getElementById("_seatsTotal");
let seatsUsed = document.getElementById("_seatsUsed");
let checkBox = document.getElementById("_useAddress")
let label= document.getElementById("_label");
let lightsOn = document.getElementById("_lights");
let heatingCoolingOn = document.getElementById("_heatingCooling");
let errorMessage = document.getElementById("errorMessages1");	
let allStorage ;
let bucket = [];
let objectAddress = [];
let storeList1=[];
let monthNum = [1,2,3,4,5,6,7,8,9,10,11,12];
let minuteNum = ["0"];

for(let i=1;i<61;i++){
    if(i < 10){
    
        minuteNum.push(`0${i}`);
   
    }else{
    
        minuteNum.push(`${i}`);
    }
}

// RoomUsage Class

class RoomUsage
{ 
    constructor(address,roomNumber,lightsOn,heatingCoolingOn,seatsUsed,seatsTotal,timeChecked)
    { 
		this._address = address
		this._roomNumber = roomNumber
        this._lightsOn = lightsOn
        this._heatingCoolingOn = heatingCoolingOn
        this._seatsUsed = seatsUsed
		this._seatsTotal = seatsTotal
		this._timeChecked = new Date()
	}
    initialiseRoomPDO(roomObject)
    {
        this._address= roomObject._address
        this._roomNUmber = roomObject._roomNumber
        this._lightsOn = roomObject._lightsOn
        this._heatingCoolingOn = roomObject._heatingCoolingOn
        this._seatsUsed = roomObject._seatsUsed
		this._seatsTotal = roomObject._seatsTotal
        this._timeChecked = new Date(roomObject._timeChecked)
    }
	get roomNumber()
    {
		return this._roomnumber;
	}// return the private variables
	get address()
    {
		return this._address; 
	}
	get seatsUsed()
    {
		return this._seatsUsed;
	}
	get seatsTotal()
    {
		return this._seatsTotal;
	}
	get timeChecked()
    {
		return this._timeChecked;
	}
    
}

// RoomUsageList Class
class roomUsageList 
{
	constructor()
    {
		this._roomList = [];
	}
}
let roomList = new roomUsageList();

myFunction()
//retrieving when loading page
function myFunction()
{
    if(localStorage.getItem("ENG1003-RoomUseList"))
    {
        roomList = JSON.parse(localStorage.getItem("ENG1003-RoomUseList"));
    }
}
//Bucketing Time
function bucketingTime()
{
    for(let i = 0; i < roomList._roomList.length; i++)
            {
                let timeCheck = new Date(roomList._roomList[i]._timeChecked);
                let timeNum = timeCheck.getHours();
                let objectTime = timeNum.toString();            

                if(objectTime < 19 && objectTime > 7)
                {
                    if (objectTime <= 12)
                    {
                        objectTime = objectTime + " am"
                    }
                    else
                    {
                        objectTime = objectTime-12 + " pm"    
                    }
                    
                    if (bucket.hasOwnProperty(objectTime))
                    {
                            bucket[objectTime]._roomList.push(roomList._roomList[i])

                    }
                    else
                    {    
                            bucket[objectTime] = new roomUsageList()
                            bucket[objectTime]._roomList.push(roomList._roomList[i])
                     }
                }
            }
    }
//Bucketing Building
function bucketingBuilding()
{
     for(let i = 0; i < roomList._roomList.length; i++)
        {
         
         objectAddress = (roomList._roomList[i]._address);
            
                if (bucket.hasOwnProperty(objectAddress))
                {
                    bucket[objectAddress]._roomList.push(roomList._roomList[i])
                    console.log(2)
                }
                else
                {    
                    bucket[objectAddress]= new roomUsageList()
                    bucket[objectAddress]._roomList.push(roomList._roomList[i])
                }
        }
         
}
//Converting true into On
function convertOnOff(something)
{
    if (something  == true || something == "On")
    {
       return  something = "On"
        
    }
    else 
    {
         return something = "Off"
        
    }
}

