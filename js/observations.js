"use strict";
let numberObservation = document.getElementById("text");
let searchField = document.getElementById("searchField");
allStorage = [];
let myHTML=''
let data = document.getElementById("content");
//Displaying the localstorage in obervation.html
if (localStorage)
{
    allStorage = JSON.parse(localStorage.getItem("ENG1003-RoomUseList"))
}
function templateData()
{
    var month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    for(let i = allStorage._roomList.length - 1; i >= 0; i--)
    {
    let time = new Date(allStorage._roomList[i]._timeChecked)    
    myHTML +=  `
    <div class="mdl-cell mdl-cell--4-col" id = "cell${i}">
    <table id="_date1" class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">
        <thead>
            <tr><th class="amdl-data-table__cell--non-numeric" id = "head">
                <h4 class="date">${time.getDate()} ${month[time.getMonth()]}.</h4>
                <h4 style = "text-align:left"> 
                    ${allStorage._roomList[i]._address.split(',')[0]}. <br />
                    Rm ${allStorage._roomList[i]._roomNumber}
                </h4>
            </th></tr>
        </thead>
        <tbody>
            <tr><td id="_data1" class="mdl-data-table__cell--non-numeric">
            Time : ${time.getHours()}:${minuteNum[time.getMinutes()]} <br />
            Light : ${convertOnOff(allStorage._roomList[i]._lightsOn)} <br />
            Heating/cooling : ${convertOnOff(allStorage._roomList[i]._heatingCoolingOn)} <br />
            Seat usage : ${allStorage._roomList[i]._seatsUsed}/${allStorage._roomList[i]._seatsTotal}<br />
                <button class="mdl-button mdl-js-button mdl-button--icon" onclick="deleteObservationAtIndex(${i});"> 
                    <i class="material-icons">delete</i> 
                </button> 
            </td></tr>
        </tbody>
    </table>
</div>
` 
    numberObservation.innerText = `${allStorage._roomList.length} Observations.`
    }
     data.innerHTML  = myHTML;
}
templateData()

//Delete button
function deleteObservationAtIndex(index) {
    document.getElementById(`cell${index}`).remove();
    allStorage._roomList.splice(index,1)
    localStorage.setItem("ENG1003-RoomUseList",JSON.stringify(allStorage))
    window.location.reload()
}
//sraching button
let input = document.querySelector("input")
input.oninput = search;

function search() {
    data.innerHTML = "";
    let newArr = [];
    for (let i = 0; i < allStorage._roomList.length; i++) {
        if (allStorage._roomList[i]._address.toLowerCase().indexOf(searchField.value.toLowerCase()) !== -1 ||
        allStorage._roomList[i]._roomNumber.toLowerCase().indexOf(searchField.value.toLowerCase()) !== -1) {
        newArr.push(allStorage._roomList[i]);
        }
    }
    function templateData2(allStorage){
    var month = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    for(let i = allStorage.length - 1; i >= 0; i--){
    let time = new Date()    
    data.innerHTML +=  `
    <div class="mdl-cell mdl-cell--4-col" id = "cell${i}">
    <table id="_date1" class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">
        <thead>
            <tr><th class="amdl-data-table__cell--non-numeric" id = "head">
                <h4 class="date">${time.getDate()} ${month[time.getMonth()]}.</h4>
                <h4 style = "text-align:left"> 
                    ${allStorage[i]._address.split(',')[0]}. <br />
                    Rm ${allStorage[i]._roomNumber}
                </h4>
            </th></tr>
        </thead>
        <tbody>
            <tr><td id="_data1" class="mdl-data-table__cell--non-numeric">
            Time : ${time.getHours()}:${minuteNum[time.getMinutes()]} <br />
            Light : ${convertOnOff(allStorage[i]._lightsOn)} <br />
            Heating/cooling : ${convertOnOff(allStorage[i]._heatingCoolingOn)} <br />
            Seat usage : ${allStorage[i]._seatsUsed}/${allStorage[i]._seatsTotal}<br />
                <button class="mdl-button mdl-js-button mdl-button--icon" onclick="deleteObservationAtIndex(${i});"> 
                    <i class="material-icons">delete</i> 
                </button> 
            </td></tr>
        </tbody>
    </table>
</div>
` 
numberObservation.innerText = `${allStorage.length} Observations.`
    }
}
    if(newArr.length > 0) {
        templateData2(newArr)
    }
}
